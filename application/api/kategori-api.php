<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["nama"])) :
			$judul   = $connect->clean_post($_POST["nama"]);

			$target_dir = "../../assets/kategor/";
			$name    = "image21";
			$image      = $_FILES["image"]["name"];
			$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		    $tar_img	= $target_dir . $newimage;
		    $upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);

		    if($image == "" || $image == null) :
		    	$query  = $connect->query("INSERT INTO tr_kategori (TK_NAMA) VALUES ('$judul')");

		    	if($query) :
		    		$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori berhasil ditambahkan";
					echo json_encode($response);
		    	else :
		    		$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori gagal ditambahkan";
					echo json_encode($response);
		    	endif;
		    else :
        		$query  = $connect->query("INSERT INTO tr_kategori (TK_NAMA, TK_IMAGE) VALUES ('$judul', '$newimage')");

        		if($query && $upload1) :
		    		$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori berhasil ditambahkan";
					echo json_encode($response);
		    	else :
		    		$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori gagal ditambahkan";
					echo json_encode($response);
		    	endif;
        	endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "edit") :
		$value  = $_GET["value"];
		if(isset($value) != "") :
			// get data gambar
			$qq   = $connect->query("SELECT TK_IMAGE FROM tr_kategori WHERE TK_BIGID = '$value'");
			$row  = $qq->fetch_assoc();
			$gmbr = $row["TK_IMAGE"];

			// parameter
			$judul  = $connect->clean_post($_POST["nama"]);
			$newname = $connect->clean_post($_POST["image"]);

			if($newname == "" || $newname == null) :
				$query = $connect->query("UPDATE tr_kategori SET TK_NAMA = '$judul' WHERE TK_BIGID = '$value'");
			else :
				$query = $connect->query("UPDATE tr_kategori SET TK_NAMA = '$judul', TK_IMAGE = '$newname' WHERE TK_BIGID = '$value'");
			endif;

			 if($query == TRUE) :
		        	$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori berhasil diubah";
					echo json_encode($response);
		        else :
		        	$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Kategori gagal diubah";
					echo json_encode($response);
		        endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Kategori tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $_GET["value"];
		if(isset($value) != "") :
			//get data image
			$qq   = $connect->query("SELECT TK_IMAGE FROM tr_kategori WHERE TK_BIGID = '$value'");
			$row  = $qq->fetch_assoc();
			$gmbr = $row["TK_IMAGE"];

			$target_dir = "../../assets/kategori/";
			$query   = $connect->query("DELETE FROM tr_kategori WHERE TK_BIGID = '$value'");

			if($query) :
				$path = $target_dir.$gmbr;
				@unlink ("$path");

				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Materi berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Materi gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Kategori tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/kategori/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);
		if($upload1 == TRUE) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	endif;
else :
	$rows  = array();
	$query = $connect->query("SELECT * FROM tr_kategori ORDER BY TK_CREATED_AT DESC");
	while($query = $query->fetch_assoc()) {
		$rows[] = $row;
	}

	if($rows == null || $rows == "") :
		$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TK_BIGID'] = "";
		$response['TK_NAMA']  = "";
		$response['TK_IMAGE'] = "";
		$response['TK_CREATED_AT'] = "";
		echo(json_encode($response));
	else :
		$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list kategori';
		$response['payload'] = $rows;
		echo(json_encode($response));
	endif;
endif;

?>