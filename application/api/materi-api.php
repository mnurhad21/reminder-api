<?php
error_reporting(0);
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');
include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["judul"]) || isset($_POST["subject"]) || isset($_POST["katId"])) :
			$judul 		= $connect->clean_post($_POST["judul"]);
			$subject	= $connect->clean_post($_POST["subject"]);
			$katId      = $connect->clean_post($_POST["katId"]);

			$target_dir = "../../assets/materi/";
			$newname = date('dmYHis').str_replace(" ", "", basename($_FILES["image"]['name']));
	        $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["image"]['name']));
	        $target_file = $target_dir . $newfilename; 
	        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        	move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

			$query = $connect->query("INSERT INTO tr_materi (TM_KATID, TM_JUDUL, TM_ISI, 	TM_GAMBAR) VALUES ('$katId', '$judul', '$subject', '$newfilename')");

			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Materi berhasil ditambahkan";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Materi gagal ditambahkan";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "edit") :
		$value = $_GET["value"];
    	if(isset($value) != "") :
    		if(isset($_POST["judul"]) || isset($_POST["subject"])) :
    			$judul   = $connect->clean_post($_POST["judul"]);
				$isi     = $connect->clean_post($_POST["subject"]);
				$newname = $connect->clean_post($_POST["image"]);
				//get tabel kategori
				$qq = $connect->query("SELECT TM_GAMBAR FROM tr_materi WHERE TM_BIGID = '$value'");
			    $qq1 = $qq->fetch_assoc();
				$newname1 = $qq1["TM_GAMBAR"];

				//update data
				if($newname == "" || $newname == null) :
					$query = $connect->query("UPDATE tr_materi SET 	TM_JUDUL = '$judul', 	TM_ISI = '$isi' WHERE TM_BIGID = '$value'");
			    else : 
			    	$query = $connect->query("UPDATE tr_materi SET 	TM_JUDUL = '$judul', 	TM_ISI = '$isi', TM_GAMBAR = '$newname' WHERE TM_BIGID = '$value'"); 
			    endif;

			    if($query == TRUE) :
		        	$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Materi berhasil diubah";
					echo json_encode($response);
		        else :
		        	$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Materi gagal diubah";
					echo json_encode($response);
		        endif;
    		else :
    			$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Parameter anda kurang";
				echo json_encode($response);
    		endif;
    	else :
    		$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Materi tidak ditemukan";
			echo json_encode($response);
    	endif;
	elseif($accesId == "delete") :
		$value = $_GET["value"];
    	if(isset($value) != "") :
    		//get tabel news
			$qq = $connect->query($conn, "SELECT TM_GAMBAR FROM tr_materi WHERE TM_BIGID = '$value'");
		    $qq1 = $qq->fetch_assoc();
			$newname = $qq1["TM_GAMBAR"];
			//gambar
			$target_dir = "../../assets/materi/";
			$query = $connect->query("DELETE FROM tr_materi WHERE TM_BIGID = '$value'");
			if($query == TRUE) :
			    $path = "../../assets/materi/".$newname;
			    @unlink ("$path");

			    $response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Materi berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Materi gagal dihapus";
				echo json_encode($response);
			endif;
    	else : 
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id kategori tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/materi/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);
		if($upload1) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	endif;
else :
	$key  = $_GET["katId"];
	if(isset($key) != "") :
		$rows  = array();
		$query = $connect->query("SELECT * FROM tr_materi WHERE TM_KATID = '$katId' ORDER BY 	TM_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		if($rows == "" || $rows == null) :
			$response["error"]  = TRUE;
    		$response["status"] = 200;
    		$response["msg"]	= "list data materi kosong";
		    $response["TM_BIGID"] =  "";
		    $response["TM_KATID"] =  "";
            $response["TM_JUDUL"] = "";
            $response["TM_ISI"]   = "";
            $response["TM_GAMBAR"] = "";
            $response["TM_CREATED_AT"] = "";
            echo json_encode($response);
		else :
			$response["error"]  = FALSE;
    		$response["status"] = 200;
    		$response["msg"]	= "list data materi";
    		$response["payload"] = $rows;
    		echo json_encode($response);
		endif;
	else :
	endif;
endif;

?>