<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $_GET["acces"];
	if($accesId == "obat") :
		$rows  = array();
		$query = $connect->query("SELECT *, COUNT(TMO_BIGID) AS jml FROM `tr_minumobat` GROUP BY TMO_USERID ORDER BY jml DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "diet") :
		$rows  = array();
		$query = $connect->query("SELECT *, COUNT(TH_USERID) AS jml FROM `tr_hipertensi` GROUP BY TH_USERID ORDER BY jml DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "aktifitas") :
		$rows  = array();
		$query = $connect->query("SELECT *, COUNT(TA_USERID) AS jml FROM `tr_aktifitas` GROUP BY TA_USERID ORDER BY jml DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	elseif($accesId == "kesehatan") :
		$rows  = array();
		$query = $connect->query("SELECT *, COUNT(TK_USERID) AS jml FROM `tr_kesehatan` GROUP BY TK_USERID ORDER BY jml DESC");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($rows);
	else :
		$response["error"]  = TRUE;
		$response["status"] = 200;
		$response["msg"]    = "Pilih dahulu akses anda";
		echo json_encode($response);
	endif;
else :
	if(isset($_GET["cari"])) :
		$rows  = array();
		$cari  = $_GET["cari"];
		$query = $connect->query("SELECT * FROM  tr_user WHERE U_NAME LIKE '%".$cari."%' OR U_FULLNAME LIKE '%".$cari."%' AND U_GROUP_RULE = 'USER'");
		while($row  = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		echo json_encode($response);
	else :
		$rows  = array();
		$query = $connect->query("SELECT * FROM  tr_user WHERE U_GROUP_RULE = 'USER' ORDER BY U_CREATED_AT DESC");
		while($row  = $query->fetch_assoc()) :
		$rows[] = $row;
		endwhile;

	    echo json_encode($rows);
	endif;
endif;
?>
