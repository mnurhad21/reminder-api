<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "ubah") :
		$value = $_GET["value"];
		if($value != "") :
			if(isset($_POST["fullname"]) || isset($_POST["tgl_lahir"]) || isset($_POST["alamat"])) :
				$fullname = $connect->clean_post($_POST["fullname"]);
				$tgl_lahir= $connect->clean_post($_POST["tgl_lahir"]);
				$gender   = $connect->clean_post($_POST["gender"]);
				$kerja    = $connect->clean_post($_POST["pekerjaan"]);
				$alamat   = $connect->clean_post($_POST["alamat"]);
				$image    = $connect->clean_post($_POST["image"]);

				if($image == "" || $image == null) :
					$query = $connect->query("UPDATE tr_user SET U_FULLNAME = '$fullname', U_TGL_LAHIR = '$tgl_lahir', U_JK = '$gender', U_PEKERJAAN = '$pekerjaan', U_ALAMAT = '$alamat' WHERE U_BIGID = '$value'");

					if($query) :
						$response["error"]  = FALSE;
						$response["status"] = 200;
						$response["msg"]	= "Profile berhasil diubah";
						echo json_encode($response);
					else :
						$response["error"]  = TRUE;
						$response["status"] = 200;
						$response["msg"]	= "Profile gagal diubah";
						echo json_encode($response);
					endif;
				else :
					$query = $connect->query("UPDATE tr_user SET U_FULLNAME = '$fullname', U_TGL_LAHIR = '$tgl_lahir', U_JK = '$gender', U_IMAGE = '$image', U_PEKERJAAN = '$pekerjaan', U_ALAMAT = '$alamat' WHERE U_BIGID = '$value'");

					if($query) :
						$response["error"]  = FALSE;
						$response["status"] = 200;
						$response["msg"]	= "Profile berhasil diubah";
						echo json_encode($response);
					else :
						$response["error"]  = TRUE;
						$response["status"] = 200;
						$response["msg"]	= "Profile gagal diubah";
						echo json_encode($response);
					endif;
				endif;
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Parameter anda kurang";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id User tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/user/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);
		if($upload1 == TRUE) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	else :
		$response["error"] = TRUE;
		$response["status"] = 200;
		$response["msg"]    = "pilih acces anda";
		echo json_encode($response);
	endif;
else :
   $value = $_GET["value"];

   if($value != "") :
   	  $rows = array();
   	  $query = $connect->query("SELECT * FROM tr_user WHERE U_BIGID = '$value'");
   	  $row   = $query->fetch_assoc();
   	  echo json_encode($row);
   else :
   	  $response["error"] = TRUE;
	  $response["status"] = 200;
	  $response["msg"]    = "Id user tidak ditemukan";
	  echo json_encode($response);
   endif;
endif;

?>