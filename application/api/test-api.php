<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

$date = "2019-05-20";
$sts  = "SELESAI";


$now  = date("Y-m-d");

if($date != $now) :
	if($sts == "SELESAI") :
		echo json_encode("START");
	endif;
else :
	echo json_encode("SELESAI");
endif;
?>