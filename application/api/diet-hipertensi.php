<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["userId"]) || isset($_POST["waktu"]) || isset($_POST["cemilan"])) :
			$userId = $connect->clean_all($_POST['userId']);
			$waktu  = $connect->clean_post($_POST['waktu']);
			$jenis  = $connect->clean_post($_POST['cemilan']);
			//tanggal
			$tgl1   = $connect->clean_post($_POST['tanggalAwal']);
			$tglAwal=date('Y-m-d', strtotime($tgl1));
			$tgl2   = $connect->clean_post($_POST['tanggalAkhir']);
			$tglAkhir=date('Y-m-d', strtotime($tgl2));

			$query  = $connect->query("INSERT INTO tr_hipertensi (TH_USERID, TH_WAKTU, TH_CEMILAN, TH_START_TGL, TH_END_TGL, TH_STATUS) VALUES ('$userId', '$waktu', '$jenis', '$tglAwal', '$tglAkhir', 'START')");

			if($query) :
				$response['error'] = FALSE;
				$response['status'] = 200;
				$response['msg'] = 'Diet Hipertensi berhasil ditambahkan';
				echo(json_encode($response));
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Diet Hipertensi gagal ditambahkan';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Parameter anda kurang';
			echo(json_encode($response));
		endif;
	elseif($accesId == "edit") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			if(isset($_POST["waktu"]) || isset($_POST["cemilan"])) :
				$waktu  = $connect->clean_post($_POST['waktu']);
				$jenis  = $connect->clean_post($_POST['cemilan']);
				//tanggal
				$tgl1   = $connect->clean_post($_POST['tanggalAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl1));
				$tgl2   = $connect->clean_post($_POST['tanggalAkhir']);
				$tglAkhir=date('Y-m-d', strtotime($tgl2));

				$query = $connect->query("UPDATE tr_hipertensi SET TH_WAKTU = '$waktu', 	TH_CEMILAN = '$jenis', TH_START_TGL = '$tglAwal', TH_END_TGL = '$tglAkhir' WHERE TH_BIGID = '$value'");
				if($query) :
					$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'Ubah Diet Hipertensi berhasil';
					echo(json_encode($response));
				else :
					$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'ubah Diet Hipertensi gagal';
					echo(json_encode($response));
				endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Diet Hipertensi tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			$query = $connect->query("DELETE FROM tr_hipertensi WHERE TH_BIGID = '$value'");
			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Diet Hipertensi berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Diet Hipertensi gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Diet Hipertensi tidak ditemukan";
			echo json_encode($response);
		endif;
	endif;
else :
  $userId = $_GET["userId"];
  if($userId != "") :
	  $rows  = array();
	  $query = $connect->query("SELECT * FROM  tr_hipertensi ORDER BY TH_CREATED_AT DESC");
	  while($row = $query->fetch_assoc()) :
	  	$rows[] = $row;
	  endwhile;

	  if($rows == "" || $rows == null):
	  	$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TH_BIGID'] = "";
		$response['TH_USERID']  = "";
		$response['TH_WAKTU']   = "";
		$response['TH_CEMILAN'] = "";
		$response['TH_START_TGL'] = "";
		$response['TH_END_TGL'] = "";
		$response['TH_STATUS'] = "";
		$response['TH_CREATED_AT'] = "";
		echo(json_encode($response));
	  else :
	  	$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list Diet Hipertensi';
		$response['payload'] = $rows;
		echo(json_encode($response));
	  endif;
  else :
    $response['error'] = TRUE;
	$response['status'] = 200;
	$response['msg'] = 'Id User tidak ditemukan';
	echo(json_encode($response));
  endif;
endif;
?>