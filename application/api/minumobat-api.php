<?php
// error_reporting(0);
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["userId"]) || isset($_POST["nama"]) || isset($_POST["dosis"])) :
			$userId = $connect->clean_post($_POST["userId"]);
			$nama   = $connect->clean_post($_POST["nama"]);
			$dosis  = $connect->clean_post($_POST["dosis"]);
		$frekuensi  = $connect->clean_post($_POST["frekuensi"]);
		    $waktu  = $connect->clean_post($_POST["waktu"]);
		    $sedia  = $connect->clean_post($_POST["sedia"]);
		    //tanggal
		    $tgl1   = $connect->clean_post($_POST["tanggalAwal"]);
		    $tglAwal= date('Y-m-d', strtotime($tgl1));
		    $tgl2   = $connect->clean_post($_POST["tanggalAkhir"]);
		    $tglAkhir= date('Y-m-d', strtotime($tgl2));

		    $query = $connect->query("INSERT INTO tr_minumobat (TMO_USERID, TMO_NAMA, TMO_DOSIS, TMO_FREKUENSI, TMO_WAKTU, TMO_START_TGL, TMO_END_TGL, TMO_KETERSEDIAAN, TMO_STATUS) VALUES ('$userId', '$nama', '$dosis', '$frekuensi', '$waktu', '$tglAwal', '$tglAkhir', '$sedia', 'START')");

		    if($query) :
		    	$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Tambah reminder berhasil";
				echo json_encode($response);
		    else :
		    	$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Tambah reminder gagal";
				echo json_encode($response);
		    endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "edit") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			$nama   = $connect->clean_post($_POST["nama"]);
			$dosis  = $connect->clean_post($_POST["dosis"]);
		    $frekuensi  = $connect->clean_post($_POST["frekuensi"]);
		    $waktu  = $connect->clean_post($_POST["waktu"]);
		    $sedia  = $connect->clean_post($_POST["sedia"]);
		    //tanggal
		    $tgl1   = $connect->clean_post($_POST["tanggalAwal"]);
		    $tglAwal= date('Y-m-d', strtotime($tgl1));
		    $tgl2   = $connect->clean_post($_POST["tanggalAkhir"]);
		    $tglAkhir= date('Y-m-d', strtotime($tgl2));

		    $query = $connect->query("UPDATE tr_minumobat SET 	TMO_NAMA = '$nama', 	TMO_DOSIS = '$dosis', TMO_FREKUENSI = '$frekuensi', TMO_WAKTU = '$waktu', 	TMO_START_TGL = '$tglAwal', TMO_END_TGL = '$tglAkhir', TMO_KETERSEDIAAN = '$sedia' WHERE TMO_BIGID = '$value'");
		    if($query) :
		    	$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Update Reminder berhasil";
				echo json_encode($response);
		    else :
		    	$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Update Reminder gagal";
				echo json_encode($response);
		    endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Reminder tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			$query = $connect->query("DELETE FROM tr_minumobat WHERE TMO_BIGID = '$value'");
			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Reminder berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Reminder gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Reminder tidak ditemukan";
			echo json_encode($response);
		endif;
	endif;
else :
	$userId = $_GET["userId"];
	if($userId != "") :
		$rows  = array();
		$query = $connect->query("SELECT * FROM tr_minumobat WHERE TMO_USERID = '$userId' ORDER BY 	TMO_CREATED_AT DESC");
		while($row = $query->fetch_assoc()) :
			$rows[] = $row;
		endwhile;

		if($rows == "" || $rows == null) :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Data tidak tersedia';
			$response['TMO_BIGID'] = "";
			$response['TMO_USERID'] = "";
			$response['TMO_NAMA'] = "";
			$response['TMO_DOSIS'] = "";
			$response['TMO_FREKUENSI'] = "";
			$response['TMO_WAKTU'] = "";
			$response['TMO_START_TGL'] = "";
			$response['TMO_END_TGL'] = "";
			$response['TMO_KETERSEDIAAN'] = "";
			$response['TMO_STATUS'] = "";
			$response['TMO_CREATED_AT'] = "";
			echo json_encode($response);
		else :
			$response["error"]  = FALSE;
			$response["status"] = 200;
			$response["msg"]	= "list reminder";
			$response["payload"]= $rows;
			echo json_encode($response);
		endif;
	else :
		$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Id User tidak ditemukan';
		echo(json_encode($response));
	endif;
endif;
?>