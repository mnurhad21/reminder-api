<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["userId"]) || isset($_POST["waktu"])) :
			$userId = $connect->clean_all($_POST['userId']);
			$waktu  = $connect->clean_post($_POST['waktu']);
			$jenis  = $connect->clean_post($_POST['jenis']);
			//tanggal
			$tgl1   = $connect->clean_post($_POST['tanggalAwal']);
			$tglAwal=date('Y-m-d', strtotime($tgl1));
			$tgl2   = $connect->clean_post($_POST['tanggalAkhir']);
			$tglAkhir=date('Y-m-d', strtotime($tgl2));

			$query  = $connect->query("INSERT INTO  tr_aktifitas (TA_USERID, TA_WAKTU, TA_JENIS, TA_START_TGL, TA_END_TGL, TA_STATUS) VALUES ('$userId', '$waktu', '$jenis', '$tglAwal', '$tglAkhir', 'START')");

			if($query) :
				$response['error'] = FALSE;
				$response['status'] = 200;
				$response['msg'] = 'Aktifitas berhasil ditambahkan';
				echo(json_encode($response));
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Aktifitas gagal ditambahkan';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Parameter anda kurang';
			echo(json_encode($response));
		endif;
	elseif($accesId == "edit") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			if(isset($_POST["waktu"])) :
				$waktu  = $connect->clean_post($_POST['waktu']);
				$jenis  = $connect->clean_post($_POST['jenis']);
				//tanggal
				$tgl1   = $connect->clean_post($_POST['tanggalAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl1));
				$tgl2   = $connect->clean_post($_POST['tanggalAkhir']);
				$tglAkhir=date('Y-m-d', strtotime($tgl2));

				$query = $connect->query("UPDATE tr_aktifitas SET TA_WAKTU = '$waktu', 	TA_JENIS = '$jenis', TA_START_TGL = '$tglAwal', TA_END_TGL = '$tglAkhir' WHERE TA_BIGID = '$value'");
				if($query) :
					$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'Ubah Aktifitas berhasil';
					echo(json_encode($response));
				else :
					$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'ubah Aktifitas gagal';
					echo(json_encode($response));
				endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Aktifitas tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			$query = $connect->query("DELETE FROM tr_aktifitas WHERE TA_BIGID = '$value'");
			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Aktifitas berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Aktifitas gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Aktifitas tidak ditemukan";
			echo json_encode($response);
		endif;
	endif;
else :
  $userId = $_GET["userId"];
  if($userId != "") :
	  $rows  = array();
	  $query = $connect->query("SELECT * FROM  tr_aktifitas ORDER BY TA_CREATED_AT DESC");
	  while($row = $query->fetch_assoc()) :
	  	$rows[] = $row;
	  endwhile;

	  if($rows == "" || $rows == null):
	  	$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TA_BIGID'] = "";
		$response['TA_USERID']  = "";
		$response['TA_WAKTU']   = "";
		$response['TA_JENIS'] = "";
		$response['TA_START_TGL'] = "";
		$response['TA_END_TGL'] = "";
		$response['TA_STATUS'] = "";
		$response['TA_CREATED_AT'] = "";
		echo(json_encode($response));
	  else :
	  	$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list Aktifitas';
		$response['payload'] = $rows;
		echo(json_encode($response));
	  endif;
  else :
    $response['error'] = TRUE;
	$response['status'] = 200;
	$response['msg'] = 'Id User tidak ditemukan';
	echo(json_encode($response));
  endif;
endif;

?>