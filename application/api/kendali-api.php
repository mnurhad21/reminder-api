<?php
error_reporting(0);
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');
include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["judul"]) || isset($_POST["subject"])) :
			$judul 		= $connect->clean_post($_POST["judul"]);
			$subject	= $connect->clean_post($_POST["subject"]);

			$target_dir = "../../assets/kendali/";
			$newname = date('dmYHis').str_replace(" ", "", basename($_FILES["image"]['name']));
	        $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["image"]['name']));
	        $target_file = $target_dir . $newfilename; 
	        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        	move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

			$query = $connect->query("INSERT INTO tr_kendali (TR_NAMA, 	TR_ISI, TR_IMAGE) VALUES ('$judul', '$subject', '$newfilename')");

			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Pengendalian berhasil ditambahkan";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Pengendalian gagal ditambahkan";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "edit") :
		$value = $_GET["value"];
    	if(isset($value) != "") :
    		if(isset($_POST["judul"]) || isset($_POST["subject"])) :
    			$judul   = $connect->clean_post($_POST["judul"]);
				$isi     = $connect->clean_post($_POST["subject"]);
				$newname = $connect->clean_post($_POST["image"]);
				//get tabel kategori
				$qq = $connect->query("SELECT TR_IMAGE FROM tr_kendali WHERE TR_BIGID = '$value'");
			    $qq1 = $qq->fetch_assoc();
				$newname1 = $qq1["TR_IMAGE"];

				//update data
				if($newname == "" || $newname == null) :
					$query = $connect->query("UPDATE tr_kendali SET TR_NAMA = '$judul', 	TR_ISI = '$isi' WHERE TR_BIGID = '$value'");
			    else : 
			    	$query = $connect->query("UPDATE tr_kendali SET TR_NAMA = '$judul', 	TR_ISI = '$isi', TR_IMAGE = '$newname' WHERE TR_BIGID = '$value'"); 
			    endif;

			    if($query == TRUE) :
		        	$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Pengendalian berhasil diubah";
					echo json_encode($response);
		        else :
		        	$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Pengendalian gagal diubah";
					echo json_encode($response);
		        endif;
    		else :
    			$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Parameter anda kurang";
				echo json_encode($response);
    		endif;
    	else :
    		$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Pengendalian tidak ditemukan";
			echo json_encode($response);
    	endif;
	elseif($accesId == "delete") :
		$value = $_GET["value"];
    	if(isset($value) != "") :
    		//get tabel news
			$qq = $connect->query("SELECT TR_IMAGE FROM tr_kendali WHERE TR_BIGID = '$value'");
		    $qq1 = $qq->fetch_assoc();
			$newname = $qq1["TM_GAMBAR"];
			//gambar
			$target_dir = "../../assets/kendali/";
			$query = $connect->query("DELETE FROM tr_kendali WHERE TR_BIGID = '$value'");
			if($query == TRUE) :
			    $path = "../../assets/kendali/".$newname;
			    @unlink ("$path");

			    $response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Pengendalian berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Pengendalian gagal dihapus";
				echo json_encode($response);
			endif;
    	else : 
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Pengendalian tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "upload") :
		$name    = "image21";
		$target_dir = "../../assets/kendali/";
		$image      = $_FILES["image"]["name"];
		$newimage = str_replace(" ", "", basename($name))."_".date('dmYHis')."_".str_replace(" ", "", basename($image));
		$tar_img	= $target_dir . $newimage;
		$upload1 = move_uploaded_file($_FILES["image"]["tmp_name"], $tar_img);
		if($upload1 == TRUE) :
			$response['error'] = FALSE;
			$response['status'] = 200;
			$response['msg'] = 'Berhail Upload Image';
			$response['image_name'] = $newimage;
			echo(json_encode($response));
		exit();
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'gagal Upload Image';
			echo(json_encode($response));
		endif;
	endif;
else :
  $rows  = array();
  $query = $connect->query("SELECT * FROM tr_kendali ORDER BY TR_CREATED_AT DESC");
  while($row = $query->fetch_assoc()) :
  	$rows[] = $row;
  endwhile;

  if($rows == "" || $rows == null):
  	$response['error'] = TRUE;
	$response['status'] = 200;
	$response['msg'] = 'Data tidak tersedia';
	$response['TR_BIGID'] = "";
	$response['TR_NAMA']  = "";
	$response['TR_ISI']   = "";
	$response['TR_IMAGE'] = "";
	$response['TR_CREATED_AT'] = "";
	echo(json_encode($response));
  else :
  	$response['error'] = FALSE;
	$response['status'] = 200;
	$response['msg'] = 'list Pengendalian';
	$response['payload'] = $rows;
	echo(json_encode($response));
  endif;
endif;

?>