<?php
error_reporting(0);
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();
$now    = date('Y-m-d');

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["jenis_olahraga"]) || isset($_POST["lama"]) || isset($_POST["merokok"]) || isset($_POST["asap"]) || isset($_POST["userId"])) :
			$userId = $connect->clean_all($_POST['userId']);
			$jenis  = $connect->clean_post($_POST['jenis_olahraga']);
			$lama   = $connect->clean_post($_POST['lama']);
			$merokok= $connect->clean_post($_POST['merokok']);
			$asap   = $connect->clean_post($_POST['asap']);
			//tanggal & waktu
			$jam    = $connect->clean_post($_POST['jam']);
			$tgl    = $connect->clean_post($_POST['tanggal']);
			$tanggal=date('Y-m-d', strtotime($tgl));

			//get data 
			$sql    = $connect->query("SELECT * FROM  tr_olahraga WHERE TO_TGL = '$now' AND TO_USERID = '$userId'");
			if(mysqli_num_rows($sql) < 0) :
				$query  = $connect->query("INSERT INTO  tr_olahraga (TO_USERID, TO_JNS_OLAHRAGA, TO_LAMA, TO_MEROKOK, 	TO_ASP_ROKOK, TO_JAM, TO_TGL, TO_STATUS) VALUES ('$userId', '$jenis', '$lama', '$merokok', '$asap', '$jam', '$tanggal', 'START')");

				if($query) :
					$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'Olahraga berhasil ditambahkan';
					echo(json_encode($response));
				else :
					$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Olahraga gagal ditambahkan';
					echo(json_encode($response));
				endif; 
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Olahraga sudah anda inputkan untuk hari ini';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Parameter anda kurang';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perhari") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			$rows  = array();
			$query = $connect->query("SELECT * FROM  tr_olahraga WHERE TO_USERID = '$userId' AND TO_TGL = '$now' ORDER BY TO_CREATED_AT DESC");
			while($row  = $query->fetch_assoc()) :
				$rows[] = $row;
			endwhile;
			if($rows == "" || $rows == null):
			  	$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Data tidak tersedia';
				$response['TO_BIGID'] = "";
				$response['TO_USERID']  = "";
				$response['TO_JNS_OLAHRAGA']   = "";
				$response['TO_LAMA'] = "";
				$response['TO_MEROKOK'] = "";
				$response['TO_ASP_ROKOK'] = "";
				$response['TO_JAM'] = "";
				$response['TO_TGL'] = "";
				$response['TO_STATUS'] = "";
				$response['TO_CREATED_AT'] = "";
				echo(json_encode($response));
			  else :
			  	$response['error'] = FALSE;
				$response['status'] = 200;
				$response['msg'] = 'list Olahraga';
				$response['payload'] = $rows;
				echo(json_encode($response));
			  endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perminggu") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			if(isset($_POST["tglAwal"]) || isset($_POST["tglAkhir"])) :
				$tgl    = $connect->clean_post($_POST['tglAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl));
				$tgl1   = $connect->clean_post($_POST["tglAkhir"]);
				$tglAkhir= date('Y-m-d', strtotime($tgl1));

				$query  = $connect->query("SELECT * FROM  tr_olahraga WHERE TO_TGL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY TO_CREATED_AT DESC");
				while($row  = $query->fetch_assoc()) :
					$rows[] = $row;
				endwhile;
				if($rows == "" || $rows == null):
				  	$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Data tidak tersedia';
					$response['TO_BIGID'] = "";
					$response['TO_USERID']  = "";
					$response['TO_JNS_OLAHRAGA']   = "";
					$response['TO_LAMA'] = "";
					$response['TO_MEROKOK'] = "";
					$response['TO_ASP_ROKOK'] = "";
					$response['TO_JAM'] = "";
					$response['TO_TGL'] = "";
					$response['TO_STATUS'] = "";
					$response['TO_CREATED_AT'] = "";
					echo(json_encode($response));
				  else :
				  	$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'list Olahraga';
					$response['payload'] = $rows;
					echo(json_encode($response));
				  endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perbulan") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			if(isset($_POST["tglAwal"]) || isset($_POST["tglAkhir"])) :
				$tgl    = $connect->clean_post($_POST['tglAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl));
				$tgl1   = $connect->clean_post($_POST["tglAkhir"]);
				$tglAkhir= date('Y-m-d', strtotime($tgl1));

				$query  = $connect->query("SELECT * FROM  tr_olahraga WHERE TO_TGL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY TO_CREATED_AT DESC");
				while($row  = $query->fetch_assoc()) :
					$rows[] = $row;
				endwhile;
				if($rows == "" || $rows == null):
				  	$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Data tidak tersedia';
					$response['TO_BIGID'] = "";
					$response['TO_USERID']  = "";
					$response['TO_JNS_OLAHRAGA']   = "";
					$response['TO_LAMA'] = "";
					$response['TO_MEROKOK'] = "";
					$response['TO_ASP_ROKOK'] = "";
					$response['TO_JAM'] = "";
					$response['TO_TGL'] = "";
					$response['TO_STATUS'] = "";
					$response['TO_CREATED_AT'] = "";
					$response['TO_CREATED_AT'] = "";
					echo(json_encode($response));
				  else :
				  	$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'list Kepatuhan';
					$response['payload'] = $rows;
					echo(json_encode($response));
				  endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	else :
		$response["error"]  = TRUE;
		$response["status"] = 200;
		$response["msg"]    = "Pilih dahulu akses anda";
		echo json_encode($response);
	endif;
else :
	$userId  = connect->clean_all($_GET['userId']);

	if($userId != "") :
		$query  = $connect->query("SELECT * FROM  tr_olahraga WHERE TO_USERID = '$userId' ORDER BY TO_CREATED_AT DESC");
	while($row  = $query->fetch_assoc()) :
		$rows[] = $row;
	endwhile;
	if($rows == "" || $rows == null):
	  	$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TO_BIGID'] = "";
		$response['TO_USERID']  = "";
		$response['TO_JNS_OLAHRAGA']   = "";
		$response['TO_LAMA'] = "";
		$response['TO_MEROKOK'] = "";
		$response['TO_ASP_ROKOK'] = "";
		$response['TO_JAM'] = "";
		$response['TO_TGL'] = "";
		$response['TO_STATUS'] = "";
		$response['TO_CREATED_AT'] = "";
		echo(json_encode($response));
	  else :
	  	$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list Olahraga';
		$response['payload'] = $rows;
		echo(json_encode($response));
	  endif;
	else :
		$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Id User Tidak ditemukan';
		echo(json_encode($response));
	endif;
endif;