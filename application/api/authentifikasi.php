<?php
ini_set('date.timezone', 'Asia/Jakarta');
header('Content-Type: application/json');
include '../model/config.php';
$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "login") :
		if(isset($_POST["loginName"]) && isset($_POST["loginPassword"])) :
			$username = $connect->clean_all($_POST["loginName"]);
		    $password = $connect->clean_all($_POST["loginPassword"]);
		    $pass     = md5(md5($password));
		    $ip       = $_SERVER['REMOTE_ADDR'];
            $browser  = $_SERVER['HTTP_USER_AGENT'];
            $token    = base64_encode(date('YmdHis').$username);
            $now      = date("Y-m-d H:i:s");

            $query    = $connect->query("SELECT * FROM tr_user WHERE U_NAME = '$username' AND U_PASSWORD = '$password'");
            if(mysqli_num_rows($query) > 0) :
            	$row    = $query->fetch_assoc($query);
            	$userId = $row["U_BIGID"];
            	//cek status user
            	if($row["U_STATUS"] == "AKTIF") :
            	    // update data user
            	    $update = $connect->query("UPDATE tr_user SET U_LOGIN_TOKEN = '$token', U_LOGIN_WAKTU = '$now', U_DEFAULT_BROWSER = '$browser', U_IP_POSITION = '$ip' WHERE U_BIGID = '$userId'");

            	    // cek berdasarkan rule 
            	    if($row["U_GROUP_RULE"] == "ADMIN") :
            	    	$response["error"]  		= FALSE;
						$response["status"] 		= 200;
						$response["bigId"]			= $row["U_BIGID"];
						$response["name"]   		= $row["U_NAME"];
						$response["fullname"] 		= $row["U_FULLNAME"];
						$response["tgl_lahir"]		= $row["U_TGL_LAHIR"];
						$response["gender"]			= $row["U_JK"];
						$response["image"]			= $row["U_IMAGE"];
						$response["pekerjaan"]      = $row["U_PEKERJAAN"];
						$response["status"]			= $row["U_STATUS"];
						$response["rule"]			= $row["U_GROUP_RULE"];
						$response["token"]			= $row["U_LOGIN_TOKEN"];
						$response["waktu"] 			= $row["U_LOGIN_WAKTU"];
						$response["browser"]		= $row["U_DEFAULT_BROWSER"];
						$response["position"]		= $row["U_IP_POSITION"];
            	    elseif($row["U_GROUP_RULE"] == "USER") :
            	    	$response["error"]  		= FALSE;
						$response["status"] 		= 200;
						$response["bigId"]			= $row["U_BIGID"];
						$response["name"]   		= $row["U_NAME"];
						$response["fullname"] 		= $row["U_FULLNAME"];
						$response["tgl_lahir"]		= $row["U_TGL_LAHIR"];
						$response["gender"]			= $row["U_JK"];
						$response["image"]			= $row["U_IMAGE"];
						$response["pekerjaan"]      = $row["U_PEKERJAAN"];
						$response["status"]			= $row["U_STATUS"];
						$response["rule"]			= $row["U_GROUP_RULE"];
						$response["token"]			= $row["U_LOGIN_TOKEN"];
						$response["waktu"] 			= $row["U_LOGIN_WAKTU"];
						$response["browser"]		= $row["U_DEFAULT_BROWSER"];
						$response["position"]		= $row["U_IP_POSITION"];
            	    else :
            	    	$response["error"]  = TRUE;
		        	    $response["status"] = 200;
		        	    $response["msg"]	= "Anda tidak memiliki akses disini";
		        	    echo json_encode($response);
            	    endif;
            	else :
            		$response["error"]  = TRUE;
		        	$response["status"] = 200;
		        	$response["msg"]	= "Akun anda tidak aktif,hubungi call center untuk info lebih lanjut";
		        	echo json_encode($response);
            	endif;
            else :
            	$response["error"]  = TRUE;
		        $response["status"] = 200;
		        $response["msg"]	= "Akun anda tidak ditemukan";
		        echo json_encode($response);
            endif;
		else :
			$response["error"]  = TRUE;
		    $response["status"] = 200;
		    $response["msg"]	= "Parameter login anda kurang";
		    echo json_encode($response);
		endif;
	elseif($accesId == "register") :
		if(isset($_POST["username"]) || isset($_POST["password"]) || isset($_POST["fullname"])) :
			$name     = $connect->clean_post($_POST["name"]);
			$fullname = $connect->clean_post($_POST["fullname"]);
			$tgl_lahir= $connect->clean_post($_POST["tgl_lahir"]);
			$jk       = $connect->clean_post($_POST["gender"]);
			$password = $connect->clean_post($_POST["password"]);
			$pass     = md5(md5($password));

			//get username
			$uname = $connect->query("SELECT * FROM tr_user WHERE U_NAME = '$name'");
			if(mysqli_num_rows($uname)) :
				$response["error"]  = TRUE;
		    	$response["status"] = 200;
		    	$response["msg"]	= "Username sudah tersedia";
		    	echo json_encode($response);
			else :
				$query    = $connect->query("INSERT tr_user (U_NAME, U_FULLNAME, U_TGL_LAHIR, U_JK, U_PEKERJAAN, U_PASSWORD, U_STATUS, U_GROUP_RULE) VALUES ('$name', '$fullname', '$tgl_lahir', '$jk', '-', '$pass', 'AKTIF', 'USER')");
				if($query) :
					$response["error"]  = FALSE;
		    		$response["status"] = 200;
		    		$response["msg"]	= "Register anda berhasil,silahkan login untuk memulai";
		    		echo json_encode($response);
				else :
					$response["error"]  = TRUE;
		    		$response["status"] = 200;
		    		$response["msg"]	= "Register anda gagal";
		    		echo json_encode($response);
				endif;
			endif;
		else :
			$response["error"]  = TRUE;
		    $response["status"] = 200;
		    $response["msg"]	= "Parameter login anda kurang";
		    echo json_encode($response);
		endif;
	else :
		$response["error"]  = TRUE;
        $response["status"] = 404;
        $response["msg"]	= "Acces anda tidak ditemukan";
        echo json_encode($response);
	endif;
else :
	$response["error"]  = TRUE;
    $response["status"] = 404;
    $response["msg"]	= "Pilih Acces anda terlebih dahulu";
    echo json_encode($response);
endif;
?>