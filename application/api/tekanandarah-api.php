<?php
error_reporting(0);
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();
$now    = date('Y-m-d');

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["distolik"]) || isset($_POST["sistolik"]) || isset($_POST["userId"])) :
			$userId = $connect->clean_all($_POST['userId']);
			$sistolik  = $connect->clean_post($_POST['sistolik']);
			$distolik   = $connect->clean_post($_POST['distolik']);

			//tanggal & waktu
			$jam    = $connect->clean_post($_POST['jam']);
			$tgl    = $connect->clean_post($_POST['tanggal']);
			$tanggal=date('Y-m-d', strtotime($tgl));

			//get data 
			$sql    = $connect->query("SELECT * FROM  tr_tekanan_darah WHERE TKD_TGL = '$now' AND TKD_USERID = '$userId'");
			if(mysqli_num_rows($sql) < 0) :
				$query  = $connect->query("INSERT INTO  tr_tekanan_darah (TKD_USERID, TKD_DIASTOLIK, TKD_SISTOLIK, TKD_JAM, TKD_TGL, TKD_STATUS) VALUES ('$userId', '$distolik', '$sistolik', '$jam', '$tanggal', 'START')");

				if($query) :
					$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'Tekanan Darah berhasil ditambahkan';
					echo(json_encode($response));
				else :
					$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Tekanan Darah gagal ditambahkan';
					echo(json_encode($response));
				endif; 
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Tekanan Darah sudah anda inputkan untuk hari ini';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Parameter anda kurang';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perhari") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			$rows  = array();
			$query = $connect->query("SELECT * FROM  tr_tekanan_darah WHERE TKD_USERID = '$userId' AND TKD_TGL = '$now' ORDER BY TKD_CREATED_AT DESC");
			while($row  = $query->fetch_assoc()) :
				$rows[] = $row;
			endwhile;
			if($rows == "" || $rows == null):
			  	$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Data tidak tersedia';
				$response['TKD_BIGID'] = "";
				$response['TKD_USERID']  = "";
				$response['TKD_DIASTOLIK']   = "";
				$response['TKD_SISTOLIK']   = "";
				$response['TKD_JAM'] = "";
				$response['TKD_TGL'] = "";
				$response['TKD_STATUS'] = "";
				$response['TKD_CREATED_AT'] = "";
				echo(json_encode($response));
			  else :
			  	$response['error'] = FALSE;
				$response['status'] = 200;
				$response['msg'] = 'list Tekanan Darah';
				$response['payload'] = $rows;
				echo(json_encode($response));
			  endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perminggu") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			if(isset($_POST["tglAwal"]) || isset($_POST["tglAkhir"])) :
				$tgl    = $connect->clean_post($_POST['tglAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl));
				$tgl1   = $connect->clean_post($_POST["tglAkhir"]);
				$tglAkhir= date('Y-m-d', strtotime($tgl1));

				$query  = $connect->query("SELECT * FROM  tr_tekanan_darah WHERE TKD_TGL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY TKD_CREATED_AT DESC");
				while($row  = $query->fetch_assoc()) :
					$rows[] = $row;
				endwhile;
				if($rows == "" || $rows == null):
				  	$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Data tidak tersedia';
					$response['TKD_BIGID'] = "";
					$response['TKD_USERID']  = "";
					$response['TKD_DIASTOLIK']   = "";
					$response['TKD_SISTOLIK']   = "";
					$response['TKD_JAM'] = "";
					$response['TKD_TGL'] = "";
					$response['TKD_STATUS'] = "";
					$response['TKD_CREATED_AT'] = "";
					echo(json_encode($response));
				  else :
				  	$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'list Tekanan Darah';
					$response['payload'] = $rows;
					echo(json_encode($response));
				  endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	elseif($accesId == "perbulan") :
		$userId  = connect->clean_all($_GET['userId']);

		if($userId != "") :
			if(isset($_POST["tglAwal"]) || isset($_POST["tglAkhir"])) :
				$tgl    = $connect->clean_post($_POST['tglAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl));
				$tgl1   = $connect->clean_post($_POST["tglAkhir"]);
				$tglAkhir= date('Y-m-d', strtotime($tgl1));

				$query  = $connect->query("SELECT * FROM  tr_tekanan_darah WHERE TKD_TGL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY TKD_CREATED_AT DESC");
				while($row  = $query->fetch_assoc()) :
					$rows[] = $row;
				endwhile;
				if($rows == "" || $rows == null):
				  	$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'Data tidak tersedia';
					$response['TKD_BIGID'] = "";
					$response['TKD_USERID']  = "";
					$response['TKD_DIASTOLIK']   = "";
					$response['TKD_SISTOLIK']   = "";
					$response['TKD_JAM'] = "";
					$response['TKD_TGL'] = "";
					$response['TKD_STATUS'] = "";
					$response['TKD_CREATED_AT'] = "";
					echo(json_encode($response));
				  else :
				  	$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'list Tekanan Darah';
					$response['payload'] = $rows;
					echo(json_encode($response));
				  endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Id User Tidak ditemukan';
			echo(json_encode($response));
		endif;
	else :
		$response["error"]  = TRUE;
		$response["status"] = 200;
		$response["msg"]    = "Pilih dahulu akses anda";
		echo json_encode($response);
	endif;
else :
	$userId  = connect->clean_all($_GET['userId']);

	if($userId != "") :
		$query  = $connect->query("SELECT * FROM  tr_tekanan_darah WHERE TKD_USERID = '$userId' ORDER BY TKD_CREATED_AT DESC");
	while($row  = $query->fetch_assoc()) :
		$rows[] = $row;
	endwhile;
	if($rows == "" || $rows == null):
	  	$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TKD_BIGID'] = "";
		$response['TKD_USERID']  = "";
		$response['TKD_DIASTOLIK']   = "";
		$response['TKD_SISTOLIK']   = "";
		$response['TKD_JAM'] = "";
		$response['TKD_TGL'] = "";
		$response['TKD_STATUS'] = "";
		$response['TKD_CREATED_AT'] = "";
		echo(json_encode($response));
	  else :
	  	$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list Tekanan Darah';
		$response['payload'] = $rows;
		echo(json_encode($response));
	  endif;
	else :
		$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Id User Tidak ditemukan';
		echo(json_encode($response));
	endif;
endif;