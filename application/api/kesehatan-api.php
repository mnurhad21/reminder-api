<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');

include '../model/config.php';

$connect     = new Connection();

if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "add") :
		if(isset($_POST["userId"]) || isset($_POST["waktu"]) || isset($_POST["tempat"])) :
			$userId = $connect->clean_all($_POST['userId']);
			$waktu  = $connect->clean_post($_POST['waktu']);
			$jenis  = $connect->clean_post($_POST['tempat']);
			//tanggal
			$tgl1   = $connect->clean_post($_POST['tanggal']);
			$tglAwal=date('Y-m-d', strtotime($tgl1));

			$query  = $connect->query("INSERT INTO tr_kesehatan (TK_USERID, TK_WAKTU, TK_TGL, TK_TEMPAT) VALUES ('$userId', '$waktu', '$tglAwal', '$jenis')");

			if($query) :
				$response['error'] = FALSE;
				$response['status'] = 200;
				$response['msg'] = 'Kesehatan berhasil ditambahkan';
				echo(json_encode($response));
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Kesehatan gagal ditambahkan';
				echo(json_encode($response));
			endif;
		else :
			$response['error'] = TRUE;
			$response['status'] = 200;
			$response['msg'] = 'Parameter anda kurang';
			echo(json_encode($response));
		endif;
	elseif($accesId == "edit") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			if(isset($_POST["waktu"]) || isset($_POST["tempat"])) :
				$waktu  = $connect->clean_post($_POST['waktu']);
				$jenis  = $connect->clean_post($_POST['tempat']);
				//tanggal
				$tgl1   = $connect->clean_post($_POST['tanggalAwal']);
				$tglAwal=date('Y-m-d', strtotime($tgl1));

				$query = $connect->query("UPDATE tr_kesehatan SET TK_WAKTU = '$waktu', TK_TGL = '$tglAwal', TK_TEMPAT = '$tempat' WHERE TK_BIGID = '$value'");
				if($query) :
					$response['error'] = FALSE;
					$response['status'] = 200;
					$response['msg'] = 'Ubah Kesehatan berhasil';
					echo(json_encode($response));
				else :
					$response['error'] = TRUE;
					$response['status'] = 200;
					$response['msg'] = 'ubah Kesehatan gagal';
					echo(json_encode($response));
				endif;
			else :
				$response['error'] = TRUE;
				$response['status'] = 200;
				$response['msg'] = 'Parameter anda kurang';
				echo(json_encode($response));
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Kesehatan tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $connect->clean_all($_GET["value"]);
		if($value != "") :
			$query = $connect->query("DELETE FROM tr_kesehatan WHERE TK_BIGID = '$value'");
			if($query) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Kesehatan berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Kesehatan gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Kesehatan tidak ditemukan";
			echo json_encode($response);
		endif;
	endif;
else :
  $userId = $_GET["userId"];
  if($userId != "") :
	  $rows  = array();
	  $query = $connect->query("SELECT * FROM  tr_kesehatan ORDER BY TK_CREATED_AT DESC");
	  while($row = $query->fetch_assoc()) :
	  	$rows[] = $row;
	  endwhile;

	  if($rows == "" || $rows == null):
	  	$response['error'] = TRUE;
		$response['status'] = 200;
		$response['msg'] = 'Data tidak tersedia';
		$response['TK_BIGID'] = "";
		$response['TK_USERID']  = "";
		$response['TK_WAKTU']   = "";
		$response['TK_TGL'] = "";
		$response['TK_TEMPAT'] = "";
		$response['TK_CREATED_AT	'] = "";
		echo(json_encode($response));
	  else :
	  	$response['error'] = FALSE;
		$response['status'] = 200;
		$response['msg'] = 'list Kesehatan';
		$response['payload'] = $rows;
		echo(json_encode($response));
	  endif;
  else :
    $response['error'] = TRUE;
	$response['status'] = 200;
	$response['msg'] = 'Id User tidak ditemukan';
	echo(json_encode($response));
  endif;
endif;
?>