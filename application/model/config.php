<?php
/**
* 
*/
class Connection
{
	// hosting
	// var $host		= "localhost";
	// var $username	= "root";
	// var $password	= "";
	// var $db			= "reminder";

	//local
	var $host		= "localhost";
	var $username	= "mitrared_reminde";
	var $password	= "dkDB21@!";
	var $db			= "mitrared_reminder";

	function conn() {
		$koneksi = mysqli_connect($this->host, $this->username, $this->password) or die("Gagal koneksi database! msg: ". mysqli_connect_error());
		$select_db = mysqli_select_db($koneksi, $this->db) or die("Database tidak ditemukan! msg: ". mysqli_error($koneksi));
		return $koneksi;
	}

	function query($request){
		return mysqli_query($this->conn(), $request);
	}
	
	function num_rows($request){
		return mysqli_num_rows(mysqli_query($this->conn(), $request));
	}

	function fetch_assoc($request) {

		return mysqli_fetch_assoc(mysqli_query($this->conn(), $request));
	}

	function clean_post($request)
	{
		return mysqli_escape_string($this->conn(), $request);
	}

	function clean_xss($request)
	{
		$xss = strip_tags($request);
		return $this->clean_post($xss);
	}

	function clean_all($request)
	{
		$post = $this->clean_post($request);
		$xss = $this->clean_xss($post);
		return $xss;
	}

	function generateRandomString($length)
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function generateRandomNumber($length)
	{
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function convertNumberRoman($number) {
	    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
	    $returnValue = '';
	    while ($number > 0) {
	        foreach ($map as $roman => $int) {
	            if($number >= $int) {
	                $number -= $int;
	                $returnValue .= $roman;
	                break;
	            }
	        }
	    }
	    return $returnValue;
	}

	function security_before_login($api_key)
	{
		if ($api_key) {
			$api 		= $this->clean_all(isset($api_key));
			$query		= "SELECT * FROM td_setting WHERE TS_KEY = '$api'";
			// if ($this->num_rows($query) == 0) {
			// 	$response['success'] = TRUE;
			// 	$response['status']  = 200;
			// 	$response['message'] = 'Api Key Not found';
			// 	echo(json_encode($response));
			// 	exit();
			// }
		} else {
			$response['success'] = TRUE;
			$response['status']  = 200;
			$response['message'] = 'Api Key Not found';
			echo(json_encode($response));
			exit();
		}
	}

	function security_after_login($api_key,$user,$sess)
	{
		if ($api_key) {
			$api 		= $this->clean_all($_GET['key']);
			$query		= "SELECT * FROM td_setting WHERE TS_KEY = '$api'";
			// if ($this->num_rows($query) == 0) {
			// 	$response['success'] = 0;
			// 	$response['message'] = 'Data dikunci';
			// 	echo(json_encode($response));
			// 	exit();
			// }
		} else {
			$response['success'] = TRUE;
			$response['status']  = 200;
			$response['message'] = 'Api Key Not found';
			echo(json_encode($response));
			exit();
		}

		if ($sess && $user) {
			$user_id	= $this->clean_all($user);
			$session	= $this->clean_all($sess);
			$query		= "SELECT * FROM td_user WHERE TU_NAME = '$user_id' AND TU_LOGIN_TOKEN = '$session'";
		} else {
			$response['success'] = TRUE;
			$response['status']  = 200;
			$response['message'] = 'Key dan token tidak ditemukan';
			echo(json_encode($response));
			exit();
		}

// 		$response['success'] = FALSE;
// 		$response['status']  = 200;
// 		$response['message'] = 'Succesful Authentifikasi';
// 		echo(json_encode($response));
// 		exit();

		//return $response;
	}

	function generateReferral($username, $length)
	{
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    $generateUser = substr(md5($username), 4, 4);
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $generateUser.$randomString;
	}

}
?>